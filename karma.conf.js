module.exports = function(config) {
  config.set({
    basePath: './',
    browsers: ['Chrome'],
    frameworks: ['jasmine'],
    files: [
      'node_modules/angular/angular.js',
      'node_modules/angular-ui-router/release/angular-ui-router.js',
      'node_modules/angular-resource/angular-resource.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'app/app.js',
      'app/**/*.module.js',
      'app/**/*.js',
      'app/**/*.spec.js'
    ],
    reporters: ['spec']
  });
};