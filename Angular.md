# Angular
* Extend current HTML5
* Make small pieces of reusable components
* Organise components into modules
* Excellent for CRUD applications
* Bad for graphics/DOM manipulation
* jQuery, bad word. Angular includes jQlite
* DOM rendering and event handling on the same thread. Different in angular 2.
* Suffers from verbosity, and confusing terms.

## Modules
* Collection of related service providers including: controllers, services, factories, directives/components, filters etc

## MV*
* You are free to use any MV pattern. Angular is built using MVVM and it is definitely easier to adopt that model. The VM in angular is the $scope object.

* The $scope object is what glues the view (DOM) to your controllers. 

* $scope is not part of angular 2 and is even hidden in later versions of angular 1

# ToDo App
# Architecture
* Back end agnostic
* Communicate over protocol
* Highly testable
* Scalable

# Gulp
* Node task runner
* Asynchronous by nature
* Plugin for nearly everything you'd want
* File concat
* Template Caching
* SCSS
* Environment Constants
* CI integration

## folder structure
Bad:

app
- controllers
- services
- directives

Good:

app
- books
-- controllers
-- services
-- directives
- employees
-- controllers
-- services
-- directives

## Bootstrapping
* Automatic bootstrapping
* Main app module

## UI Router
* Third party. Highly supported by the angular-ui team
* Better than the standard router

## Module declarations
* Declare your routes and entry points
* Organise your related services
* Should be loaded in your main app module
* Tests? Styles?

## Templates/Partials
* The View.

## Controllers
* What are they?
* When to use?
* 
## Components/Directives
* Why Components?

## Services
* Factory Vs Service
* ToDo Service
* Resource Factory

## Filters
* Priority Filter

# Testing
* Karma
* Jasmine
* Injection
* gulp integration

# SCSS
* Use it.
* It's not difficult.
* It's a superset of CSS3


