angular.module('templates', []);
angular.module('ngTut', [
  'ui.router',
  'ngResource',
  'shared',
  'templates',
  'todos'
]);

angular.module('shared', ['ui.router']);

'use_strict';

angular.module('test', ['ui.router'])
.config(function($stateProvider) {
  let state = {
    name: 'test',
    url: '/test',
    template: '<h3>TEST</h3>'
  };

  $stateProvider.state(state);
});

angular.module('todos', ['ui.router']).config(function($stateProvider) {
  let state = {
    name: 'todos',
    url: '/todos',
    templateUrl: 'todos/todos.tpl.html'
  };

  $stateProvider.state(state);
});

angular.module('shared').component('appNav', {
  templateUrl: 'shared/components/nav.tpl.html',
  controller: function() {
  }
});

angular.module('todos').component('todoList', {
  templateUrl: 'todos/components/todoList.tpl.html',
  controller: function($scope, ToDoService) {
    ToDoService.fetchTodos(function(err, res) {
      if (err) {
        return;
      }

      $scope.todos = res;
    });

    $scope.addTodo = function(description, priority) {
      ToDoService.addTodo(description, priority);
      delete $scope.description;
      delete $scope.priority;
    };

    $scope.$watch(
      function() {
        return ToDoService.getTodos();
      },
      function(newVal) {
        $scope.todos = newVal;
      },
      true);
  }
});

angular.module('todos').directive('todo', function() {
  return {
    scope: {
      item: '='
    },
    templateUrl: 'todos/directives/todo.tpl.html',
    controller: function($scope, ToDoService) {
      $scope.remove = function(id) {
        ToDoService.deleteTodo(id);
      };
    }
  };
});

angular.module('todos').filter('Priority', function() {
  return function(input) {
    if (input === 1) {
      return 'High';
    } else if (input === 2) {
      return 'Medium';
    } else if (input === 3) {
      return 'Low';
    }

    return input;
  };
});

angular.module('todos').service('ToDoService', function(
  TodoResource
  ) {
  let todos = [];

  this.addTodo = function(description, priority) {
    const id = Date.now();
    let todo = {
      id,
      description,
      priority
    };
    todos.push(todo);
  };

  this.getTodos = function() {
    return todos;
  };

  this.fetchTodos = function(cb) {
    return TodoResource.get(null, null, function(res) {
      todos = res;
      cb(null, todos);
    });
  };

  this.deleteTodo = function(id) {
    todos = todos.filter(n => n.id !== id);
  };
});

angular.module('todos').factory('TodoResource', function($resource) {
  return $resource('http://localhost:8888/todos', { id: '@id' }, {
    get: { method: 'GET', isArray: true },
    create: { method: 'POST' },
    put: { method: 'PUT' },
    delete: { method: 'DELETE' }
  });
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5qcyIsInNoYXJlZC9zaGFyZWQubW9kdWxlLmpzIiwidGVzdC90ZXN0Lm1vZHVsZS5qcyIsInRvZG9zL3RvZG9zLm1vZHVsZS5qcyIsInNoYXJlZC9jb21wb25lbnRzL25hdi5jb21wb25lbnQuanMiLCJ0b2Rvcy9jb21wb25lbnRzL3RvZG9MaXN0LmNvbXBvbmVudC5qcyIsInRvZG9zL2RpcmVjdGl2ZXMvdG9kby5kaXJlY3RpdmUuanMiLCJ0b2Rvcy9maWx0ZXJzL3ByaW9yaXR5LmZpbHRlci5qcyIsInRvZG9zL3NlcnZpY2VzL3RvZG8uc2VydmljZS5qcyIsInRvZG9zL3NlcnZpY2VzL3RvZG9SZXNvdXJjZS5zZXJ2aWNlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1JBO0FBQ0E7QUNEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1pBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDVEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDTEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMzQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNiQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiYW5ndWxhci5tb2R1bGUoJ3RlbXBsYXRlcycsIFtdKTtcbmFuZ3VsYXIubW9kdWxlKCduZ1R1dCcsIFtcbiAgJ3VpLnJvdXRlcicsXG4gICduZ1Jlc291cmNlJyxcbiAgJ3NoYXJlZCcsXG4gICd0ZW1wbGF0ZXMnLFxuICAndG9kb3MnXG5dKTtcbiIsImFuZ3VsYXIubW9kdWxlKCdzaGFyZWQnLCBbJ3VpLnJvdXRlciddKTtcbiIsIid1c2Vfc3RyaWN0JztcblxuYW5ndWxhci5tb2R1bGUoJ3Rlc3QnLCBbJ3VpLnJvdXRlciddKVxuLmNvbmZpZyhmdW5jdGlvbigkc3RhdGVQcm92aWRlcikge1xuICBsZXQgc3RhdGUgPSB7XG4gICAgbmFtZTogJ3Rlc3QnLFxuICAgIHVybDogJy90ZXN0JyxcbiAgICB0ZW1wbGF0ZTogJzxoMz5URVNUPC9oMz4nXG4gIH07XG5cbiAgJHN0YXRlUHJvdmlkZXIuc3RhdGUoc3RhdGUpO1xufSk7XG4iLCJhbmd1bGFyLm1vZHVsZSgndG9kb3MnLCBbJ3VpLnJvdXRlciddKS5jb25maWcoZnVuY3Rpb24oJHN0YXRlUHJvdmlkZXIpIHtcbiAgbGV0IHN0YXRlID0ge1xuICAgIG5hbWU6ICd0b2RvcycsXG4gICAgdXJsOiAnL3RvZG9zJyxcbiAgICB0ZW1wbGF0ZVVybDogJ3RvZG9zL3RvZG9zLnRwbC5odG1sJ1xuICB9O1xuXG4gICRzdGF0ZVByb3ZpZGVyLnN0YXRlKHN0YXRlKTtcbn0pO1xuIiwiYW5ndWxhci5tb2R1bGUoJ3NoYXJlZCcpLmNvbXBvbmVudCgnYXBwTmF2Jywge1xuICB0ZW1wbGF0ZVVybDogJ3NoYXJlZC9jb21wb25lbnRzL25hdi50cGwuaHRtbCcsXG4gIGNvbnRyb2xsZXI6IGZ1bmN0aW9uKCkge1xuICB9XG59KTtcbiIsImFuZ3VsYXIubW9kdWxlKCd0b2RvcycpLmNvbXBvbmVudCgndG9kb0xpc3QnLCB7XG4gIHRlbXBsYXRlVXJsOiAndG9kb3MvY29tcG9uZW50cy90b2RvTGlzdC50cGwuaHRtbCcsXG4gIGNvbnRyb2xsZXI6IGZ1bmN0aW9uKCRzY29wZSwgVG9Eb1NlcnZpY2UpIHtcbiAgICBUb0RvU2VydmljZS5mZXRjaFRvZG9zKGZ1bmN0aW9uKGVyciwgcmVzKSB7XG4gICAgICBpZiAoZXJyKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgJHNjb3BlLnRvZG9zID0gcmVzO1xuICAgIH0pO1xuXG4gICAgJHNjb3BlLmFkZFRvZG8gPSBmdW5jdGlvbihkZXNjcmlwdGlvbiwgcHJpb3JpdHkpIHtcbiAgICAgIFRvRG9TZXJ2aWNlLmFkZFRvZG8oZGVzY3JpcHRpb24sIHByaW9yaXR5KTtcbiAgICAgIGRlbGV0ZSAkc2NvcGUuZGVzY3JpcHRpb247XG4gICAgICBkZWxldGUgJHNjb3BlLnByaW9yaXR5O1xuICAgIH07XG5cbiAgICAkc2NvcGUuJHdhdGNoKFxuICAgICAgZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiBUb0RvU2VydmljZS5nZXRUb2RvcygpO1xuICAgICAgfSxcbiAgICAgIGZ1bmN0aW9uKG5ld1ZhbCkge1xuICAgICAgICAkc2NvcGUudG9kb3MgPSBuZXdWYWw7XG4gICAgICB9LFxuICAgICAgdHJ1ZSk7XG4gIH1cbn0pO1xuIiwiYW5ndWxhci5tb2R1bGUoJ3RvZG9zJykuZGlyZWN0aXZlKCd0b2RvJywgZnVuY3Rpb24oKSB7XG4gIHJldHVybiB7XG4gICAgc2NvcGU6IHtcbiAgICAgIGl0ZW06ICc9J1xuICAgIH0sXG4gICAgdGVtcGxhdGVVcmw6ICd0b2Rvcy9kaXJlY3RpdmVzL3RvZG8udHBsLmh0bWwnLFxuICAgIGNvbnRyb2xsZXI6IGZ1bmN0aW9uKCRzY29wZSwgVG9Eb1NlcnZpY2UpIHtcbiAgICAgICRzY29wZS5yZW1vdmUgPSBmdW5jdGlvbihpZCkge1xuICAgICAgICBUb0RvU2VydmljZS5kZWxldGVUb2RvKGlkKTtcbiAgICAgIH07XG4gICAgfVxuICB9O1xufSk7XG4iLCJhbmd1bGFyLm1vZHVsZSgndG9kb3MnKS5maWx0ZXIoJ1ByaW9yaXR5JywgZnVuY3Rpb24oKSB7XG4gIHJldHVybiBmdW5jdGlvbihpbnB1dCkge1xuICAgIGlmIChpbnB1dCA9PT0gMSkge1xuICAgICAgcmV0dXJuICdIaWdoJztcbiAgICB9IGVsc2UgaWYgKGlucHV0ID09PSAyKSB7XG4gICAgICByZXR1cm4gJ01lZGl1bSc7XG4gICAgfSBlbHNlIGlmIChpbnB1dCA9PT0gMykge1xuICAgICAgcmV0dXJuICdMb3cnO1xuICAgIH1cblxuICAgIHJldHVybiBpbnB1dDtcbiAgfTtcbn0pO1xuIiwiYW5ndWxhci5tb2R1bGUoJ3RvZG9zJykuc2VydmljZSgnVG9Eb1NlcnZpY2UnLCBmdW5jdGlvbihcbiAgVG9kb1Jlc291cmNlXG4gICkge1xuICBsZXQgdG9kb3MgPSBbXTtcblxuICB0aGlzLmFkZFRvZG8gPSBmdW5jdGlvbihkZXNjcmlwdGlvbiwgcHJpb3JpdHkpIHtcbiAgICBjb25zdCBpZCA9IERhdGUubm93KCk7XG4gICAgbGV0IHRvZG8gPSB7XG4gICAgICBpZCxcbiAgICAgIGRlc2NyaXB0aW9uLFxuICAgICAgcHJpb3JpdHlcbiAgICB9O1xuICAgIHRvZG9zLnB1c2godG9kbyk7XG4gIH07XG5cbiAgdGhpcy5nZXRUb2RvcyA9IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiB0b2RvcztcbiAgfTtcblxuICB0aGlzLmZldGNoVG9kb3MgPSBmdW5jdGlvbihjYikge1xuICAgIHJldHVybiBUb2RvUmVzb3VyY2UuZ2V0KG51bGwsIG51bGwsIGZ1bmN0aW9uKHJlcykge1xuICAgICAgdG9kb3MgPSByZXM7XG4gICAgICBjYihudWxsLCB0b2Rvcyk7XG4gICAgfSk7XG4gIH07XG5cbiAgdGhpcy5kZWxldGVUb2RvID0gZnVuY3Rpb24oaWQpIHtcbiAgICB0b2RvcyA9IHRvZG9zLmZpbHRlcihuID0+IG4uaWQgIT09IGlkKTtcbiAgfTtcbn0pO1xuIiwiYW5ndWxhci5tb2R1bGUoJ3RvZG9zJykuZmFjdG9yeSgnVG9kb1Jlc291cmNlJywgZnVuY3Rpb24oJHJlc291cmNlKSB7XG4gIHJldHVybiAkcmVzb3VyY2UoJ2h0dHA6Ly9sb2NhbGhvc3Q6ODg4OC90b2RvcycsIHsgaWQ6ICdAaWQnIH0sIHtcbiAgICBnZXQ6IHsgbWV0aG9kOiAnR0VUJywgaXNBcnJheTogdHJ1ZSB9LFxuICAgIGNyZWF0ZTogeyBtZXRob2Q6ICdQT1NUJyB9LFxuICAgIHB1dDogeyBtZXRob2Q6ICdQVVQnIH0sXG4gICAgZGVsZXRlOiB7IG1ldGhvZDogJ0RFTEVURScgfVxuICB9KTtcbn0pO1xuIl19
