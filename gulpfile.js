const gulp = require('gulp');
const concat = require('gulp-concat');
const inject = require('gulp-inject');
const del = require('del');
const connect = require('gulp-connect')
const watch = require('gulp-watch');
const livereload = require('gulp-livereload');
const runSequence = require('run-sequence')
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const templateCache = require('gulp-angular-templatecache');
const Server = require('karma').Server;

const vendorFiles = {
  js:[
    'node_modules/angular/angular.js',
    'node_modules/angular-ui-router/release/angular-ui-router.js',
    'node_modules/angular-resource/angular-resource.js'
    ],
  fonts: [
    'node_modules/font-awesome/fonts/*'
  ]
};

gulp.task('run', function (cb) {
  runSequence(
    'build',
    'watch',
    'serve',
    cb);
});

gulp.task('build', cb => {
  runSequence(
    'sass',
    'fonts',
    'scripts:app',
    'scripts:vendor',
    'unit',
    'templates',
    'index',
    cb)
});

gulp.task('clean', cb => {
  del('build', { force:true })
  cb()
});

gulp.task('scripts:app', () => {
  return gulp.src('./app/**/!(*.spec).js')
    .pipe(sourcemaps.init())
    .pipe(concat('app.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./build/scripts/'));
});

gulp.task('scripts:vendor', () => {
  return gulp.src(vendorFiles.js)
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('./build/scripts/'));
});

gulp.task('fonts', () => {
  return gulp.src(vendorFiles.fonts)
    .pipe(gulp.dest('./build/fonts/'));
});

gulp.task('index', () => {
  const target = gulp.src('./app/index.html');
  const sources = gulp.src([
    './build/scripts/vendor.js', 
    './build/scripts/app.js',
    './build/scripts/templates.js',
    './build/styles/main.css'], {read: false});
  const opts = {
    addRootSlash: false,
    ignorePath: 'build',
  }
  return target.pipe(inject(sources, opts))
    .pipe(gulp.dest('./build'))
    .pipe(livereload());
});

gulp.task('templates', () => {
  return gulp.src('app/**/*.tpl.html')
    .pipe(templateCache())
    .pipe(gulp.dest('./build/scripts'));
});

gulp.task('serve', () => {
  connect.server({
    root: './build/'
  });
})

gulp.task('watch',  () => {
  livereload.listen({
    reloadPage: './build/index.html'
  });

  gulp.watch(['app/**/*.js', 'app/**/*.html', 'app/**/*.scss'], ['build'])
  .on('change', event => {
    console.log('File changed: ' + event.path);
  });
});

gulp.task('sass', () => {
  return gulp.src('./app/styles/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./build/styles'));
});

gulp.task('reload', () => {
  livereload();
})

gulp.task('unit', (done) => {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true
  }, done).start();
});