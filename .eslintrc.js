module.exports = {
    "extends": ["airbnb-base", "eslint-config-es5"],
    "plugins": [
        "import"
    ],
    "globals": {
        "angular": true,
        "describe": true,
        "beforeEach": true,
        "inject": true,
        "it": true,
        "expect": true
    },
     "rules": {
        "func-names": 0,
        "prefer-arrow-callback": 0,
        "object-shorthand": 0,
        "no-param-reassign": 0,
        "prefer-const": 0
    }
};