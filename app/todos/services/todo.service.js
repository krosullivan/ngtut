angular.module('todos').service('ToDoService', function(
  TodoResource
  ) {
  let todos = [];

  this.addTodo = function(description, priority) {
    const id = Date.now();
    let todo = {
      id,
      description,
      priority
    };
    todos.push(todo);
  };

  this.getTodos = function() {
    return todos;
  };

  this.fetchTodos = function(cb) {
    return TodoResource.get(null, null, function(res) {
      todos = res;
      cb(null, todos);
    });
  };

  this.deleteTodo = function(id) {
    todos = todos.filter(n => n.id !== id);
  };
});
