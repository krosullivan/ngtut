angular.module('todos').factory('TodoResource', function($resource) {
  return $resource('http://localhost:8888/todos', { id: '@id' }, {
    get: { method: 'GET', isArray: true },
    create: { method: 'POST' },
    put: { method: 'PUT' },
    delete: { method: 'DELETE' }
  });
});
