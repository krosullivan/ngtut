angular.module('todos').filter('Priority', function() {
  return function(input) {
    if (input === 1) {
      return 'High';
    } else if (input === 2) {
      return 'Medium';
    } else if (input === 3) {
      return 'Low';
    }

    return input;
  };
});
