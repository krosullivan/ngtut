angular.module('todos').component('todoList', {
  templateUrl: 'todos/components/todoList.tpl.html',
  controller: function($scope, ToDoService) {
    ToDoService.fetchTodos(function(err, res) {
      if (err) {
        return;
      }

      $scope.todos = res;
    });

    $scope.addTodo = function(description, priority) {
      ToDoService.addTodo(description, priority);
      delete $scope.description;
      delete $scope.priority;
    };

    $scope.$watch(
      function() {
        return ToDoService.getTodos();
      },
      function(newVal) {
        $scope.todos = newVal;
      },
      true);
  }
});
