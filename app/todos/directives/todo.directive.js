angular.module('todos').directive('todo', function() {
  return {
    scope: {
      item: '='
    },
    templateUrl: 'todos/directives/todo.tpl.html',
    controller: function($scope, ToDoService) {
      $scope.remove = function(id) {
        ToDoService.deleteTodo(id);
      };
    }
  };
});
