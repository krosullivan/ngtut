angular.module('todos', ['ui.router']).config(function($stateProvider) {
  let state = {
    name: 'todos',
    url: '/todos',
    templateUrl: 'todos/todos.tpl.html'
  };

  $stateProvider.state(state);
});
