describe('todoServiceTests', function() {
  let toDoService;
  beforeEach(module('ngTut'));

  beforeEach(inject(function(ToDoService) {
    toDoService = ToDoService;
  }));

  it('should get empty list of todos', function() {
    expect(toDoService.getTodos().length).toBe(0);
  });

  it('should create a todo', function() {
    toDoService.addTodo('test', 1);
    expect(toDoService.getTodos().length).toBe(1);
    expect(toDoService.getTodos()[0].description).toBe('test');
    expect(toDoService.getTodos()[0].priority).toBe(1);
  });
});
