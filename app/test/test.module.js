'use_strict';

angular.module('test', ['ui.router'])
.config(function($stateProvider) {
  let state = {
    name: 'test',
    url: '/test',
    template: '<h3>TEST</h3>'
  };

  $stateProvider.state(state);
});
