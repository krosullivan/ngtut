angular.module('templates', []);
angular.module('ngTut', [
  'ui.router',
  'ngResource',
  'shared',
  'templates',
  'todos'
]);
